import com.ail.core.Attribute;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.util.Functions;
import com.ail.insurance.policy.Policy;

public class ValidateRouteService {
    public static void invoke(ExecutePageActionArgument args) {
        Policy policy = (Policy) args.getModelArgRet();

        Attribute mode = (Attribute) policy.xpathGet("asset/attribute[@id='modeOfTransport']");
        Attribute dest = (Attribute) policy.xpathGet("asset/attribute[@id='destination']");

        if (dest.getValue().equals("USA") && mode.getValue().equals("Road")) {
        	Attribute error = new Attribute("error.ValidateCompanyDetailService", "Bad route", "string");
            mode.addAttribute(error);
            dest.addAttribute(error);
            args.setValidationFailedRet(true);
        }
        else {
            Functions.removeErrorMarkers(mode);
            Functions.removeErrorMarkers(dest);
        }
    }
}
