import com.ail.core.Attribute;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.util.Functions;
import com.ail.insurance.policy.Policy;

public class InitiateCancellationService {
    public static void invoke(ExecutePageActionArgument args) {
        Policy policy = (Policy) args.getModelArgRet();

        policy.getLabel().add("Pending Cancellation");
    }
}
